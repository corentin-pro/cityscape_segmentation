import tensorflow as tf

from config.model_config import ModelConfig
from src.tf1_utils.layers import Layer
from src.tf1_utils.network import Network


class UNetwork(Network):
    def __init__(self, input_op, output_dim, is_training=False):
        self.input_op = input_op
        self.layers = []

        if is_training is not None:
            Layer.IS_TRAINING = is_training

        Layer.ACTIVATION = tf.nn.leaky_relu

        network_output = input_op
        input_shape = input_op.shape.as_list()
        conv1 = self.conv2D(network_output, ModelConfig.CONV1_FEATURES,
                            kernel_size=ModelConfig.KERNEL_SIZE_1, strides=[1, 2, 2, 1])
        conv1_shape = conv1.shape.as_list()
        conv2 = self.conv2D(conv1, ModelConfig.CONV2_FEATURES, strides=[1, 2, 2, 1])
        conv2_shape = conv2.shape.as_list()
        conv3 = self.conv2D(conv2, ModelConfig.CONV3_FEATURES, strides=[1, 2, 2, 1])
        conv3_shape = conv3.shape.as_list()
        conv4 = self.conv2D(conv3, ModelConfig.CONV4_FEATURES)
        # conv4_shape = conv4.shape.as_list()
        conv5 = self.conv2D(conv4, ModelConfig.CONV5_FEATURES, strides=[1, 2, 2, 1])
        conv5_shape = conv5.shape.as_list()
        conv6 = self.conv2D(conv5, ModelConfig.CONV6_FEATURES)
        # conv6_shape = conv6.shape.as_list()
        conv7 = self.conv2D(conv6, ModelConfig.CONV7_FEATURES, strides=[1, 2, 2, 1])
        conv7_shape = conv7.shape.as_list()
        conv8 = self.conv2D(conv7, ModelConfig.CONV8_FEATURES, strides=[1, 2, 2, 1])
        conv8_shape = conv8.shape.as_list()
        conv9 = self.conv2D(conv8, ModelConfig.CONV9_FEATURES, strides=[1, 2, 2, 1])
        # conv9_shape = conv9.shape.as_list()
        network_output = conv9

        output_shape = network_output.shape.as_list()
        flat_dimension = output_shape[1] * output_shape[2] * output_shape[3]
        network_output = tf.reshape(network_output, [-1, flat_dimension])

        network_output = self.fully_connected(network_output, flat_dimension // 2)
        network_output = self.fully_connected(network_output, flat_dimension)

        network_output = tf.reshape(network_output, [-1, *output_shape[1:]])

        network_output = self.deconv2D(network_output, output_shape=conv8_shape, strides=[1, 2, 2, 1])
        network_output = self.skip_connection(network_output, conv8)

        network_output = self.conv2D(network_output, ModelConfig.CONV8_FEATURES)
        network_output = self.deconv2D(network_output, output_shape=conv7_shape, strides=[1, 2, 2, 1])
        network_output = self.skip_connection(network_output, conv7)

        network_output = self.conv2D(network_output, ModelConfig.CONV7_FEATURES)
        network_output = self.deconv2D(network_output, output_shape=conv5_shape, strides=[1, 2, 2, 1])
        network_output = self.skip_connection(network_output, conv5)

        network_output = self.conv2D(network_output, ModelConfig.CONV5_FEATURES)
        network_output = self.deconv2D(network_output, output_shape=conv3_shape, strides=[1, 2, 2, 1])
        network_output = self.skip_connection(network_output, conv3, stop_grad=True)

        network_output = self.conv2D(network_output, ModelConfig.CONV3_FEATURES)
        network_output = self.deconv2D(network_output, output_shape=conv2_shape, strides=[1, 2, 2, 1])
        network_output = self.skip_connection(network_output, conv2, stop_grad=True)

        network_output = self.conv2D(network_output, ModelConfig.CONV2_FEATURES)
        network_output = self.deconv2D(network_output, output_shape=conv1_shape, strides=[1, 2, 2, 1])
        # network_output = self.skip_connection(network_output, conv1, stop_grad=True)

        network_output = self.conv2D(network_output, output_dim * 4)
        network_output = self.deconv2D(network_output, output_shape=input_shape,
                                       output_features=output_dim * 2, strides=[1, 2, 2, 1])
        network_output = self.conv2D(network_output, output_dim, batch_norm=False, activation=None)

        self.output_op = network_output
