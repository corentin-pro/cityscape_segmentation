import glob
import os
import resource
import shutil
import subprocess
import time

import tensorflow as tf
import tensorflow.compat.v1 as tf1

from config.model_config import ModelConfig
from config.train_config import TrainConfig
from src.network import UNetwork
from src.tf1_utils.layers import Layer, l1_loss
from src.tf1_utils.utils.batch_generator import BatchGenerator
from src.tf1_utils.utils.logger import DummyLogger
from src.utils.cityscape import CityscapeData


def train(batch_generator_train: BatchGenerator, batch_generator_val: BatchGenerator, input_shape: tuple,
          logger=DummyLogger(), output_dir=None):
    gpu_options = tf1.GPUOptions(allow_growth=True)
    session_config = tf1.ConfigProto(gpu_options=gpu_options)

    with tf1.Session(config=session_config).as_default() as session:
        data_placeholder = tf1.placeholder(
            tf.uint8,
            shape=[None, *input_shape],
            name='data_placeholder')
        label_placeholder = tf1.placeholder(
            tf.uint8,
            shape=[None, *input_shape[:-1]],
            name='label_placeholder')
        augment_placeholder = tf1.placeholder(tf.bool, shape=[], name='augment_placeholder')
        bn_train_placeholder = tf1.placeholder(tf.bool, shape=[], name='bn_train_placeholder')

        train_summaries = []
        image_summaries = []

        with tf1.variable_scope('PreProcess'):
            with tf1.variable_scope('DataAugment'):
                stacked_data = tf.concat([
                    data_placeholder, tf1.expand_dims(label_placeholder, axis=-1)], axis=-1)
                augmented_data_op = tf.cond(
                    augment_placeholder,
                    lambda: tf.image.random_flip_left_right(stacked_data),
                    lambda: stacked_data)
                label_op = augmented_data_op[:, :, :, -1]
                label_hot_op = tf.one_hot(label_op, TrainConfig.OUTPUT_CLASSES + 1)
                data_op = augmented_data_op[:, :, :, :-1]
                data_op = tf.cond(
                    augment_placeholder,
                    lambda: tf.image.random_contrast(
                        tf.image.random_brightness(
                            tf.image.random_hue(data_op, 0.1),
                            0.2), 0.8, 1.2),
                    lambda: data_op)
            # label_op = tf.cast(label_op, tf.float32) / 255.0
            data_op = tf.cast(data_op, tf.float32)  / 255.0

        with tf1.variable_scope('Network'):
            Layer.VERBOSE = False
            Layer.LOGGER = logger
            Layer.METRICS = False
            Layer.BATCH_NORM_TRAINING = bn_train_placeholder
            Layer.BATCH_NORM_DECAY = 0.998
            Layer.REGULARIZER = tf.nn.l2_loss
            network = UNetwork(data_op, TrainConfig.OUTPUT_CLASSES, is_training=True)

        with tf1.variable_scope('Train'):
            batch_size_op = tf.shape(data_placeholder)[0]
            expanded_prediction = tf.concat(
                [network.output_op, tf.zeros([batch_size_op, input_shape[0], input_shape[1], 1])],
                axis=3,
                name="expanded_prediction")
            prediction_argmax = tf.argmax(network.output_op, axis=-1, name="prediction_argmax")
            labels_argmax = tf.argmax(label_hot_op, axis=-1, name="labels_argmax")
            unlabeled_mask = label_hot_op[:, :, :, -1]

            with tf1.variable_scope("ColoredImages"):
                image_summaries.append(tf1.summary.image(
                    "input", data_op, max_outputs=TrainConfig.IMAGE_MAX_OUTPUT))
                label_colors = [label[7] for label in CityscapeData.TRAIN_LABELS]  # + [(0, 0, 0)]
                def color_output(output_argmax):
                    color_output = tf.one_hot(output_argmax, TrainConfig.OUTPUT_CLASSES + 1)
                    colored_label_image = tf.cast(tf.equal(
                        tf.reduce_max(color_output, axis=-1, keepdims=True), color_output), tf.uint8)
                    return tf.reduce_sum(
                        tf.multiply(tf.stack([colored_label_image] * 3, axis=-1), label_colors), axis=-2)

                masked_prediction = tf.concat(
                    [tf.nn.softmax(network.output_op, axis=-1), tf1.expand_dims(unlabeled_mask, axis=-1)], axis=3)
                masked_prediction_argmax = tf.argmax(masked_prediction, axis=-1)
                image_summaries.append(tf1.summary.image(
                    "colored_output", color_output(masked_prediction_argmax), max_outputs=TrainConfig.IMAGE_MAX_OUTPUT))
                image_summaries.append(tf1.summary.image(
                    "colored_label", color_output(labels_argmax), max_outputs=TrainConfig.IMAGE_MAX_OUTPUT))

            with tf1.variable_scope('Loss'):
                raw_loss_op = tf.nn.softmax_cross_entropy_with_logits_v2(
                    labels=tf.cast(label_hot_op, tf.float32),
                    logits=expanded_prediction)
                masked_loss_op = tf.multiply(raw_loss_op, (1 - unlabeled_mask))
                masked_loss_op = tf.reduce_mean(masked_loss_op, axis=[1, 2])

                # Regularization losses
                regularization_coef = 1e-3
                regularization_losses = tf.reduce_sum(
                    [regularization_coef * reg_op for reg_op in tf1.get_collection(tf1.GraphKeys.REGULARIZATION_LOSSES)])
                train_summaries.append(tf1.summary.scalar('reg_loss', regularization_losses, family='Loss'))

                loss_op = tf.reduce_mean(masked_loss_op) + tf.reduce_sum(regularization_losses)
                mean_loss_op, mean_loss_update_op = tf1.metrics.mean(loss_op)
                train_summaries.append(tf1.summary.scalar('loss', mean_loss_op, family='Loss'))

            with tf1.variable_scope('Optimizer'):
                step_op = tf.Variable(0, trainable=False, name="Step")
                # learning_rate_op = tf1.train.exponential_decay(
                #     TrainConfig.LEARNING_RATE, step_op, batch_generator_train.step_per_epoch // 10, 0.99)
                # momentum_op = 0.6 - tf1.train.exponential_decay(
                #     0.6, step_op, batch_generator_train.step_per_epoch // 10, 0.99)
                # train_summaries.append(tf1.summary.scalar('learning_rate', learning_rate_op))
                # train_summaries.append(tf1.summary.scalar('momentum', momentum_op))

                # optimizer_op = tf1.train.MomentumOptimizer(learning_rate_op, momentum_op)
                optimizer_op = tf1.train.AdamOptimizer(TrainConfig.LEARNING_RATE)
                gradients_op = optimizer_op.compute_gradients(
                    loss_op, var_list=tf1.trainable_variables(scope='Network'))
                def train_with_bn():
                    with tf.control_dependencies(tf1.get_collection(tf1.GraphKeys.UPDATE_OPS)):
                        return optimizer_op.apply_gradients(gradients_op, global_step=step_op)
                train_op = tf.cond(
                    bn_train_placeholder,
                    train_with_bn,
                    lambda: optimizer_op.apply_gradients(gradients_op, global_step=step_op))

            with tf1.variable_scope("Accuracy"):
                correct_pixels = tf.cast(
                    tf.logical_or(tf.cast(unlabeled_mask, dtype=tf.bool), tf.equal(prediction_argmax, labels_argmax)),
                    dtype=tf.float32)
                acc_op = tf.reduce_sum(correct_pixels) / (
                    (input_shape[0] * input_shape[1] * TrainConfig.BATCH_SIZE))
                mean_acc_op, mean_acc_update_op = tf1.metrics.mean(acc_op)
                train_summaries.append(tf1.summary.scalar('accuracy', mean_acc_op, family='Accuracy'))

            with tf1.variable_scope("IoU"):
                masked_labels = tf.cast(
                    label_hot_op * (1 - tf.expand_dims(unlabeled_mask, axis=-1)),
                    dtype=tf.int64)
                prediction_maxs = tf.cast(
                    tf.equal(expanded_prediction,
                             tf.reduce_max(expanded_prediction, axis=-1, keepdims=True)),
                    dtype=tf.int64)
                inter = tf.minimum(prediction_maxs, masked_labels)
                union = tf.maximum(prediction_maxs, masked_labels)
                accumulated_inter = tf1.get_variable(
                    'accumulated_inter',
                    shape=expanded_prediction.shape.as_list()[-1], dtype=tf.int64,
                    initializer=tf1.zeros_initializer(), trainable=False,
                    collections=[tf1.GraphKeys.LOCAL_VARIABLES])
                accumulated_union = tf1.get_variable(
                    'accumulated_union',
                    shape=expanded_prediction.shape.as_list()[-1], dtype=tf.int64,
                    initializer=tf1.zeros_initializer(), trainable=False,
                    collections=[tf1.GraphKeys.LOCAL_VARIABLES])
                update_inter = tf1.assign(
                    accumulated_inter, accumulated_inter + tf.reduce_sum(inter, [0, 1, 2]))
                update_union = tf1.assign(
                    accumulated_union, accumulated_union + tf.reduce_sum(union, [0, 1, 2]))
                update_iou_op = tf.group(update_union, update_inter)

                # IoU per class
                class_ious = tf.unstack(
                    tf.math.divide(accumulated_inter + 1,
                                   accumulated_union + 1), axis=-1)
                for label in CityscapeData.TRAIN_LABELS[:-1]:
                    train_summaries.append(tf1.summary.scalar(label[0], class_ious[label[2]], family='IoU'))
                mean_iou_op = tf.reduce_mean(tf.stack(class_ious))
                train_summaries.append(tf1.summary.scalar('mean_iou', mean_iou_op, family='Accuracy'))

        local_train_init_op = tf1.initializers.variables(tf1.local_variables('Train'))
        session.run(tf1.global_variables_initializer())
        session.run(tf1.local_variables_initializer())

        # Training loop
        checkpoints = []

        if output_dir:
            summary_op = tf1.summary.merge(
                train_summaries + [layer.summaries.histograms for layer in network.layers])
            train_writer = tf1.summary.FileWriter(
                os.path.join(output_dir, 'train'), session.graph, flush_secs=20)
            val_writer = tf1.summary.FileWriter(
                os.path.join(output_dir, 'val'), session.graph, flush_secs=20)
            image_op = tf1.summary.merge(image_summaries)
            saver = tf1.train.Saver(tf1.global_variables(scope="Network"), max_to_keep=100)

            def get_var_info(var: tf.Variable):
                var_size = 1
                for size in var.shape:
                    var_size *= size
                var_size *= 1 if var.dtype == tf.uint8 else 4
                var_unit = 'B'
                if var_size > 1024:
                    var_size = var_size // 1024
                    var_unit = 'kiB'
                if var_size > 1024:
                    var_size = var_size // 1024
                    var_unit = 'MiB'
                return '{:42};{:16};{};{}{}'.format(var.name, str(var.shape), var.dtype, var_size, var_unit)
            with open(os.path.join(output_dir, 'saved_vars.csv'), 'w') as save_file:
                save_file.write(
                    '\n'.join([get_var_info(var) for var in tf1.global_variables(scope="Network")]))

            # Save source files
            for entry in glob.glob(os.path.join('config', '**', '*.py'), recursive=True) + glob.glob(
                    os.path.join('src', '**', '*.py'), recursive=True):
                dirname = os.path.join(output_dir, 'code', os.path.dirname(entry))
                if not os.path.exists(dirname):
                    os.makedirs(dirname)
                shutil.copy2(entry, os.path.join(output_dir, 'code', entry))
        else:
            image_op = None
            train_writer = None
            val_writer = None

        logger.info('Training... ({} samples, {} steps per epoch)'.format(
            len(batch_generator_train.data), batch_generator_train.step_per_epoch))
        summary_period = int(batch_generator_train.step_per_epoch / TrainConfig.SUMMARIES_PER_EPOCH)
        if summary_period == 0:
            summary_period = 1
        image_period = int(batch_generator_train.step_per_epoch / TrainConfig.SUMMARIES_PER_EPOCH)
        if image_period == 0:
            image_period = 1

        train_start_time = time.time()
        benchmark_time = time.time()
        benchmark_step = 0

        # from tensorflow.python.framework.graph_util import convert_variables_to_constants
        # initial_graph = convert_variables_to_constants(session, session.graph_def, ['Network/output/add'])

        # tf.train.write_graph(initial_graph, output_dir, 'initial_graph.txt', as_text=True)
        try:
            # from tensorflow.python.client import timeline
            # Initialize batch generators
            if batch_generator_train.batch_data is None:
                batch_generator_train.next_batch()
            if batch_generator_val.batch_data is None:
                batch_generator_val.next_batch()

            def save_summary(force_save=False):
                nonlocal batch_generator_train
                nonlocal batch_generator_val
                nonlocal data_placeholder
                nonlocal label_placeholder
                nonlocal augment_placeholder
                nonlocal bn_train_placeholder
                nonlocal train_writer
                nonlocal val_writer
                nonlocal checkpoints
                nonlocal image_period
                nonlocal image_op

                image_needed = batch_generator_train.global_step % image_period == (image_period - 1)
                if train_writer:
                    # Training metrics
                    feed_dict = {
                        data_placeholder: batch_generator_train.batch_data,
                        label_placeholder: batch_generator_train.batch_label,
                        augment_placeholder: True,
                        bn_train_placeholder: False}
                    if image_needed:
                        train_loss, train_iou, summary, image_summary = session.run(
                            [mean_loss_op, mean_iou_op, summary_op, image_op], feed_dict=feed_dict)
                        train_writer.add_summary(image_summary, global_step=batch_generator_train.global_step)
                    else:
                        train_loss, train_iou, summary = session.run(
                            [mean_loss_op, mean_iou_op, summary_op], feed_dict=feed_dict)
                    train_writer.add_summary(summary, global_step=batch_generator_train.global_step)
                    session.run(local_train_init_op)

                    # Validation metrics
                    val_epoch = batch_generator_val.epoch
                    while val_epoch == batch_generator_val.epoch:
                        session.run(
                            [mean_loss_update_op, update_iou_op, mean_acc_update_op],
                            feed_dict={
                                data_placeholder: batch_generator_val.batch_data,
                                label_placeholder: batch_generator_val.batch_label,
                                augment_placeholder: False,
                                bn_train_placeholder: False})
                        batch_generator_val.next_batch()
                    feed_dict = {
                        data_placeholder: batch_generator_val.batch_data,
                        label_placeholder: batch_generator_val.batch_label,
                        augment_placeholder: False,
                        bn_train_placeholder: False}
                    if image_needed:
                        val_loss, val_iou, summary, image_summary = session.run(
                            [mean_loss_op, mean_iou_op, summary_op, image_op],
                            feed_dict=feed_dict)
                        val_writer.add_summary(image_summary, global_step=batch_generator_train.global_step)
                    else:
                        val_loss, val_iou, summary = session.run(
                            [mean_loss_op, mean_iou_op, summary_op],
                            feed_dict=feed_dict)
                    val_writer.add_summary(summary, global_step=batch_generator_train.global_step)
                    session.run(local_train_init_op)
                else:
                    train_loss, train_iou = session.run(
                        [mean_loss_op, mean_iou_op],
                        feed_dict={
                            data_placeholder: batch_generator_train.batch_data,
                            label_placeholder: batch_generator_train.batch_label,
                            augment_placeholder: True,
                            bn_train_placeholder: False})
                    session.run(local_train_init_op)
                    val_loss = None
                    val_iou = None
                min_iou = checkpoints[-1]['iou'] if checkpoints else -1
                final_iou = val_iou if val_iou else train_iou
                if force_save or final_iou > min_iou:
                    checkpoint_name = 'checkpoint_iou_{:0.03f}'.format(final_iou)
                    if len(checkpoints) > TrainConfig.MAX_CHECKPOINT:
                        removing_checkpoint = checkpoints.pop()
                        checkpoint_folder_content = os.listdir(output_dir)
                        for entry in checkpoint_folder_content:
                            if entry.startswith(removing_checkpoint['name']):
                                os.remove(os.path.join(output_dir, entry))
                    checkpoints.append({
                        'iou': final_iou,
                        'name': checkpoint_name
                    })
                    saver.save(
                        session, os.path.join(output_dir, checkpoint_name),
                        global_step=batch_generator_train.global_step)
                    checkpoints.sort(key=lambda x: x["iou"], reverse=True)

                speed = benchmark_step / (time.time() - benchmark_time)
                if val_loss is not None:
                    print(
                        'Global step {:d}, loss: {:.03E} {:.03E}, iou {:.03f} {:.03f}, {:0.02f} steps/s, {:0.02f} input/sec'.format(
                            batch_generator_train.global_step, train_loss, val_loss, train_iou, val_iou,
                            speed, speed * batch_generator_train.batch_size))
                else:
                    print(
                        'Global step {:d}, loss: {:.03E}, iou {:.03f}, {:0.02f} steps/s, {:0.02f} input/sec'.format(
                            batch_generator_train.global_step, train_loss, train_iou,
                            speed, speed * batch_generator_train.batch_size))

            if saver:
                saver.save(session, os.path.join(output_dir, 'checkpoint_0'))
            while batch_generator_train.epoch <= TrainConfig.TRAIN_EPOCH:
                print('Epoch {}'.format(batch_generator_train.epoch) + (
                    ' ' * (os.get_terminal_size().columns - (6 + len(str(batch_generator_train.epoch))))))
                session.run(local_train_init_op)
                epoch = batch_generator_train.epoch
                while epoch == batch_generator_train.epoch:
                    if benchmark_step > 1:
                        speed = benchmark_step / (time.time() - benchmark_time)
                        print('Step {:>4}, {:0.02f}steps/s, {:0.02f} input/sec'.format(
                            batch_generator_train.step, speed, speed * batch_generator_train.batch_size), end='\r')

                    session.run(
                        [train_op, mean_loss_update_op, update_iou_op, mean_acc_update_op],
                        feed_dict={
                            data_placeholder: batch_generator_train.batch_data,
                            label_placeholder: batch_generator_train.batch_label,
                            augment_placeholder: True,
                            bn_train_placeholder: True})
                    if ((epoch >= TrainConfig.SKIP_FIRST_EPOCHS - 1) and(
                            batch_generator_train.global_step % summary_period) == (summary_period - 1)):
                        save_summary()
                        benchmark_time = time.time()
                        benchmark_step = 0
                    batch_generator_train.next_batch()
                    benchmark_step += 1
        except KeyboardInterrupt:
            pass
        for _ in range(30):
            session.run(
                [mean_loss_update_op, update_iou_op, mean_acc_update_op],
                feed_dict={
                    data_placeholder: batch_generator_train.batch_data,
                    label_placeholder: batch_generator_train.batch_label,
                    augment_placeholder: True,
                    bn_train_placeholder: True})
            batch_generator_train.next_batch()
        save_summary(force_save=True)

        # trained_graph = convert_variables_to_constants(session, session.graph_def, ['Network/output/add'])

        # tf.train.write_graph(trained_graph, output_dir, 'trained_graph.txt', as_text=True)

        train_stop_time = time.time()

        train_memory_peak = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
        train_gpu_memory = subprocess.check_output(
            "nvidia-smi --query-gpu=memory.used --format=csv,noheader", shell=True).decode()

        if "CUDA_VISIBLE_DEVICES" in os.environ:
            train_gpu_memory = train_gpu_memory.split("\n")[int(os.environ["CUDA_VISIBLE_DEVICES"])]
        else:
            train_gpu_memory = " ".join(train_gpu_memory.split("\n"))

        result = "Training time : %gs\n\tRAM peak : %g MB\n\tVRAM usage : %s" % (
            train_stop_time - train_start_time,
            int(train_memory_peak / 1024),
            train_gpu_memory)
        logger.info(result)
