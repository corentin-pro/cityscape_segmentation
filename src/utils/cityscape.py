import os

import numpy as np


class CityscapeData:
    # pylint: disable=C0326,C0301
    LABELS = [
        #  name                     id    trainId   category         catId     hasInstances   ignoreInEval   color
        (  'unlabeled'            ,  0 ,      255 , 'void'         , 0       , False        , True         , (200,200,200) ),
        (  'ego vehicle'          ,  1 ,      255 , 'void'         , 0       , False        , True         , (  0,  0,  0) ),
        (  'rectification border' ,  2 ,      255 , 'void'         , 0       , False        , True         , (  0,  0,  0) ),
        (  'out of roi'           ,  3 ,      255 , 'void'         , 0       , False        , True         , (  0,  0,  0) ),
        (  'static'               ,  4 ,      255 , 'void'         , 0       , False        , True         , (  0,  0,  0) ),
        (  'dynamic'              ,  5 ,      255 , 'void'         , 0       , False        , True         , (111, 74,  0) ),
        (  'ground'               ,  6 ,      255 , 'void'         , 0       , False        , True         , ( 81,  0, 81) ),
        (  'road'                 ,  7 ,        0 , 'flat'         , 1       , False        , False        , (128, 64,128) ),
        (  'sidewalk'             ,  8 ,        1 , 'flat'         , 1       , False        , False        , (244, 35,232) ),
        (  'parking'              ,  9 ,      255 , 'flat'         , 1       , False        , True         , (250,170,160) ),
        (  'rail track'           , 10 ,      255 , 'flat'         , 1       , False        , True         , (230,150,140) ),
        (  'building'             , 11 ,        2 , 'construction' , 2       , False        , False        , ( 70, 70, 70) ),
        (  'wall'                 , 12 ,        3 , 'construction' , 2       , False        , False        , (102,102,156) ),
        (  'fence'                , 13 ,        4 , 'construction' , 2       , False        , False        , (190,153,153) ),
        (  'guard rail'           , 14 ,      255 , 'construction' , 2       , False        , True         , (180,165,180) ),
        (  'bridge'               , 15 ,      255 , 'construction' , 2       , False        , True         , (150,100,100) ),
        (  'tunnel'               , 16 ,      255 , 'construction' , 2       , False        , True         , (150,120, 90) ),
        (  'pole'                 , 17 ,        5 , 'object'       , 3       , False        , False        , (153,153,153) ),
        (  'polegroup'            , 18 ,      255 , 'object'       , 3       , False        , True         , (153,153,153) ),
        (  'traffic light'        , 19 ,        6 , 'object'       , 3       , False        , False        , (250,170, 30) ),
        (  'traffic sign'         , 20 ,        7 , 'object'       , 3       , False        , False        , (220,220,  0) ),
        (  'vegetation'           , 21 ,        8 , 'nature'       , 4       , False        , False        , (107,142, 35) ),
        (  'terrain'              , 22 ,        9 , 'nature'       , 4       , False        , False        , (152,251,152) ),
        (  'sky'                  , 23 ,       10 , 'sky'          , 5       , False        , False        , ( 70,130,180) ),
        (  'person'               , 24 ,       11 , 'human'        , 6       , True         , False        , (220, 20, 60) ),
        (  'rider'                , 25 ,       12 , 'human'        , 6       , True         , False        , (255,  0,  0) ),
        (  'car'                  , 26 ,       13 , 'vehicle'      , 7       , True         , False        , (  0,  0,142) ),
        (  'truck'                , 27 ,       14 , 'vehicle'      , 7       , True         , False        , (  0,  0, 70) ),
        (  'bus'                  , 28 ,       15 , 'vehicle'      , 7       , True         , False        , (  0, 60,100) ),
        (  'caravan'              , 29 ,      255 , 'vehicle'      , 7       , True         , True         , (  0,  0, 90) ),
        (  'trailer'              , 30 ,      255 , 'vehicle'      , 7       , True         , True         , (  0,  0,110) ),
        (  'train'                , 31 ,       16 , 'vehicle'      , 7       , True         , False        , (  0, 80,100) ),
        (  'motorcycle'           , 32 ,       17 , 'vehicle'      , 7       , True         , False        , (  0,  0,230) ),
        (  'bicycle'              , 33 ,       18 , 'vehicle'      , 7       , True         , False        , (119, 11, 32) ),
        (  'license plate'        , -1 ,       -1 , 'vehicle'      , 7       , False        , True         , (  0,  0,142) ),
    ]
    SPLITS = ['train', 'test', 'val']  # "train", "test", "val"
    LABEL_DIR = 'gtFine'
    IMAGE_DIR = 'leftImg8bit'

    TRAIN_COLORS = [label[7] for label in LABELS if not label[6]] + [(0, 0, 0)]
    TRAIN_LABELS = [label for label in LABELS if not label[6]] + [(*LABELS[0][:2], 19, *LABELS[0][3:7], (0, 0, 0))]


    @staticmethod
    def load_data(data_dir, split_list, resize=None):
        import multiprocessing as mp
        from PIL import Image
        import pymp
        image_count = len(split_list)

        shared_images = pymp.shared.list([[] for _ in range(image_count)])
        shared_labels = pymp.shared.list([[] for _ in range(image_count)])

        def label_id(pixel_color):
            for label in CityscapeData.TRAIN_LABELS:
                # if matching color
                if label[7][0] == pixel_color[0] and label[7][1] == pixel_color[1] and label[7][2]  == pixel_color[2]:
                    return label[2]  # returns train id
            return CityscapeData.TRAIN_LABELS[-1][2]  # default to last label train id

        process_iterate = mp.Value('d', 0)
        process_lock = mp.Lock()
        with pymp.Parallel(int(mp.cpu_count() * 0.8)) as process:
            for index in process.range(0, image_count):
                entry_data = split_list[index]
                with process_lock:
                    process_iterate.value = process_iterate.value + 1
                    print('{}/{}'.format(int(process_iterate.value), image_count), end='\r')
                if resize:
                    image_array = np.asarray(Image.open(
                        os.path.join(data_dir, entry_data[0])).convert('RGB').resize(resize, Image.NEAREST))
                else:
                    image_array = np.asarray(Image.open(os.path.join(data_dir, entry_data[0])).convert('RGB'))
                shared_images[index] = np.asarray(image_array)

                if resize:
                    label_array = np.asarray(Image.open(
                        os.path.join(data_dir, entry_data[1])).convert('RGB').resize(resize, Image.NEAREST))
                else:
                    label_array = np.asarray(Image.open(os.path.join(data_dir, entry_data[1])).convert("RGB"))
                label_ids = []
                for label_row in label_array:
                    row_ids = []
                    for pixel_color in label_row:
                        row_ids.append(int(label_id(pixel_color)))
                    label_ids.append(row_ids)
                shared_labels[index] = np.asarray(label_ids, dtype=np.uint8)
        print()

        split_images = np.asarray(shared_images)
        split_labels = np.asarray(shared_labels)

        return split_images, split_labels

    @staticmethod
    def generate_data_dict(data_dir):
        assert os.path.exists(data_dir)

        data_dict = {}
        for split_name in CityscapeData.SPLITS:
            data_dict[split_name] = []
            split_path = os.path.join(data_dir, CityscapeData.LABEL_DIR, split_name)
            split_content = os.listdir(split_path)
            for city_name in split_content:
                data_dict[split_name] += [(
                    os.path.join(
                        CityscapeData.IMAGE_DIR,
                        split_name, city_name,
                        '_'.join(entry.split('_')[:3]) + '_leftImg8bit.png'),
                    os.path.join(CityscapeData.LABEL_DIR, split_name, city_name, entry)) for entry in os.listdir(
                        os.path.join(split_path, city_name)) if entry.endswith('_color.png')]
                assert os.path.exists(os.path.join(data_dir, data_dict[split_name][0][0]))
                assert os.path.exists(os.path.join(data_dir, data_dict[split_name][0][1]))
        return data_dict
