# coding: utf-8

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import os


def save_model():
    """..."""
    parser = argparse.ArgumentParser()
    parser.add_argument('checkpoint', help='Folder to load checkpoint')
    args = parser.parse_args()

    import tensorflow as tf

    from src.network import UNetwork
    from config.train_config import TrainConfig

    if not args.checkpoint:
        print("Need checkpoint")
        return

    # manage memory(True->if we need then use, False->use all.This is same as default)
    gpu_options = tf.GPUOptions(allow_growth=True)
    session_config = tf.ConfigProto(gpu_options=gpu_options)

    with tf.Session(config=session_config).as_default() as sess:
        # Setting the variables
        symbolic_input = tf.placeholder(
            tf.float32,
            shape=[1, TrainConfig.INPUT_SHAPE[0], TrainConfig.INPUT_SHAPE[1], TrainConfig.INPUT_SHAPE[2]],
            name="symbolic_input")

        with tf.variable_scope("Normalization"):
            normalized_input = symbolic_input / 255.0

        with tf.variable_scope("Network"):
            network_output = UNetwork(normalized_input, TrainConfig.OUTPUT_CLASSES, False)
            logits = network_output.output_op

        with tf.variable_scope("Threshold"):
            softmax = tf.nn.softmax(logits, axis=3)
            thresholded = tf.to_float(softmax > TrainConfig.THRESHOLD)
            output = tf.multiply(thresholded, 255.0, name="final_output")
        # background_output, label_output = tf.split(output, 2, axis=-1)

        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())

        saver = tf.train.Saver(tf.global_variables())
        saver.restore(sess, args.checkpoint)

        frozen_graph = tf.graph_util.convert_variables_to_constants(
            sess,
            sess.graph.as_graph_def(),
            ["Threshold/final_output"]  # "Threshold/mul", "Network/DeConv7/1x1/Batch_Norm/batch_norm/add_1"
        )

        checkpoint_folder = os.path.dirname(args.checkpoint)
        # checkpoint_name = os.path.basename(args.checkpoint)

        tf.train.write_graph(frozen_graph, os.path.join(checkpoint_folder, "frozen"), 'model.pb', as_text=False)
        writer = tf.summary.FileWriter(os.path.join(checkpoint_folder, "frozen"), frozen_graph, flush_secs=1)
        writer.flush()


if __name__ == '__main__':
    save_model()
