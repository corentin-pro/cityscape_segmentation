# Cityscape Segmentation

This project is a showcase of segmentation using cityscape dataset.


## Installation

### Dataset

The dataset can found download from the official website : https://www.cityscapes-dataset.com/ . From the download page `gtFine_trainvaltest.zip`and `leftImg8bit_trainvaltest.zip` are needed.

### Dependencies

The project is coded for Python3, some packages are needed (can be installed from `pip`) :

* pillow
* pymp-pypi
* tensorflow (tensorflow-gpu recommended)
* numpy (should be a dependency of tensorflow already)

A `requirements.txt` can help installing the needed packages easily with the command :

```
pip install -r requirements.txt
```

## Usage

### Training

A typical command to train for the first time is :

```
python3 train.py --data [path/to/dataset] --json [path/filename.json] --pickle [path/filename] --resize [width]x[height]
```

As an example :

```
python3 train.py --data /storage/dataset/cityscape --json /storage/dataset/cityscape/data.json --pickle ./pickle/512x256 --resize 512x256
```

Once pickle file is created the following command is enough :

```
python3 train.py --pickle ~/dataset/cityscape/pickle/512x256
```

For any script the help message can be seen with `--help`.
