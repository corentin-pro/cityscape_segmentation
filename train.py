import argparse
import json
import os
import shutil

import numpy as np

from config.train_config import TrainConfig
from src.utils.cityscape import CityscapeData
from src.tf1_utils.utils.batch_generator import BatchGenerator
from src.tf1_utils.utils.logger import create_logger


def main():
    """..."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--output', default='output', help='Folder to save checkpoint and event')
    parser.add_argument('--data', nargs="?", help='Data directory to read from')
    parser.add_argument('--json', nargs="?", help='JSON file for training')
    parser.add_argument('--pickle', nargs="?", help='Pickle file for training')
    parser.add_argument('--resize', nargs="?",
                        help='Resize image before saving to pickle file (format : 100x200)')
    arguments = parser.parse_args()

    final_dict = {}
    if arguments.json and arguments.data:
        if os.path.exists(arguments.json):
            with open(arguments.json, "r") as data_file:
                data_dict = json.loads(data_file.read())
        else:
            data_dict = CityscapeData.generate_data_dict(arguments.data)
            with open(arguments.json, "w") as data_file:
                data_file.write(json.dumps(data_dict))
        for split_name in data_dict:
            print("Processing " + split_name + " (" + str(len(data_dict[split_name])) + ")...")
            resize = None if arguments.resize is None else [int(value) for value in arguments.resize.split('x')]
            final_dict[split_name] = CityscapeData.load_data(
                arguments.data, data_dict[split_name], resize=resize)
            if len(final_dict[split_name][0]) != len(final_dict[split_name][1]):
                print("Image and label numbers are different ({} and {})".format(
                    len(final_dict[split_name][0]), len(final_dict[split_name][1])))
                exit(1)
            if arguments.pickle:
                dirname = os.path.dirname(arguments.pickle)
                if not os.path.exists(dirname):
                    os.makedirs(dirname)
                print("Saving the {} data".format(final_dict[split_name][0].shape[0]))
                np.save(arguments.pickle + "_" + split_name + "_image.npy", final_dict[split_name][0])
                print("Saving the {} labels".format(final_dict[split_name][1].shape[0]))
                np.save(arguments.pickle + "_" + split_name + "_label.npy", final_dict[split_name][1])
    elif arguments.pickle:
        for split_name in ['train', 'val']:
            print("Loading " + split_name + "...")
            final_dict[split_name] = [
                np.load(arguments.pickle + "_" + split_name + "_image.npy"),
                np.load(arguments.pickle + "_" + split_name + "_label.npy")
            ]
        print('Data and labels loaded : {}'.format(final_dict['train'][0].shape))
    else:
        print("Need data or pickle to load data")
        return

    if os.path.exists(arguments.output):
        shutil.rmtree(arguments.output)
    os.makedirs(arguments.output)

    batch_generator_train = BatchGenerator(
        final_dict['train'][0], final_dict['train'][1], TrainConfig.BATCH_SIZE,
        preload=True)
    batch_generator_val = BatchGenerator(
        final_dict['val'][0], final_dict['val'][1], TrainConfig.BATCH_SIZE,
        preload=True)

    if batch_generator_train.batch_data is None:
        batch_generator_train.next_batch()
    if batch_generator_val.batch_data is None:
        batch_generator_val.next_batch()
    input_shape = batch_generator_train.batch_data.shape[1:]


    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    from src.train import train

    # Remove tensorflow weird logging
    import logging
    import absl.logging
    logging.root.removeHandler(absl.logging._absl_handler)
    absl.logging._warn_preinit_stderr = 0

    logger = create_logger('train', 'logs', stdout=True)
    logger.info('Starting training')
    train(batch_generator_train, batch_generator_val, input_shape, logger, arguments.output)

if __name__ == '__main__':
    main()
